from shutil import copy2
from os.path import join, exists, splitext
import json
import math
import sqlite3
from tqdm import tqdm
from pathlib import Path
from datetime import datetime
from babel.dates import format_datetime

double_images = json.load(open('double_images.json', 'r'))
output_entry_point = '/run/media/jeremiasf/Linux/newMamaOutput'

conn = sqlite3.connect('example.db')
c = conn.cursor()
firstID = c.execute('''SELECT id FROM images order by id limit 1''').fetchone()[0]
lastID = c.execute('''SELECT id FROM images order by id desc limit 1''').fetchone()[0]


def get_same_images(image_id):
    for sameImages in double_images:
        if image_id in sameImages:
            return sameImages
    return []


def get_image_from_database(image_id):
    sql = '''SELECT filepath, captured FROM images WHERE id=?'''
    return c.execute(sql, (image_id,)).fetchone()


def get_valid_filename(filename, path, count=0):
    filename_no_type, extension = splitext(filename)
    if count == 0:
        addition = ''
    else:
        addition = ' (' + str(count) + ')'
    filename_formatted = filename_no_type + addition + extension
    if exists(join(path, filename_formatted)):
        count += 1
        return get_valid_filename(filename, path, count)
    return filename_formatted


already_sorted_list = []

try:
    already_sorted_list = json.load(open('already_sorted_list.json', 'r'))
except:
    pass

for image in tqdm(range(firstID, lastID + 1)):
    if image not in already_sorted_list:
        filepath, captured = get_image_from_database(image)
        if len(captured) > 1:
            datetime = datetime.fromtimestamp(math.floor(float(captured)))
        else:
            datetime = datetime.fromtimestamp(0)

        new_path = join(output_entry_point, format_datetime(datetime, 'y', locale='de_DE'),
                        format_datetime(datetime, '(M) MMMM', locale='de_DE'))
        new_file_name = format_datetime(datetime, 'dd-HH:mm:ss', locale='de_DE') + '.jpg'

        Path(new_path).mkdir(parents=True, exist_ok=True)
        new_file_name = get_valid_filename(new_file_name, new_path)
        copy2(filepath, join(new_path, new_file_name))
        same_images = get_same_images(image)
        already_sorted_list.extend([image, *same_images])


try:
    json.dump(already_sorted_list, open('already_sorted_list.json', 'w+'))
except:
    pass

