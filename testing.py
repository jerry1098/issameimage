from filehash import FileHash
from datetime import datetime
import math
import sqlite3
from babel.dates import format_datetime
from os.path import exists, join, splitext
from shutil import copy2
from pathlib import Path
from os import stat
from PIL import Image
from PIL.ExifTags import TAGS
import matplotlib.pyplot as plt
import json
import cv2
from skimage.metrics import peak_signal_noise_ratio as psnr
from tqdm.contrib.concurrent import thread_map, process_map
import time


results = json.load(open('results.json', 'r'))
better_results = []

for result in results:
    if len(result) > 0:
        better_results.append(result[0])

print(better_results)
