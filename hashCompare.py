import sqlite3
from tqdm import tqdm
import json


def getHashFromID(imageID):
    sql = '''SELECT hash FROM images WHERE id=?'''
    return c.execute(sql, (imageID,)).fetchone()[0]


conn = sqlite3.connect('example.db')
c = conn.cursor()

firstID = c.execute('''SELECT id FROM images order by id limit 1''').fetchone()[0]
lastID = c.execute('''SELECT id FROM images order by id desc limit 1''').fetchone()[0]

hashes = c.execute('''SELECT hash, id FROM images''').fetchall()

double_images = []
double_images_unsorted = []
compared_hashes = []

for i, firstTuple in enumerate(tqdm(hashes)):
    if firstTuple[1] not in double_images_unsorted:
        matching_ids = []
        for j, secondTuple in enumerate(hashes[i + 1:]):
            if str(firstTuple[0]) == str(secondTuple[0]):
                matching_ids.append(secondTuple[1])
        if len(matching_ids) > 0:
            double_images.append((firstTuple[1], *matching_ids))
        double_images_unsorted.extend(matching_ids)
        double_images_unsorted.append(firstTuple[1])


print(len(double_images))

json.dump(double_images, open('double_images.json', 'w+'), indent=2)
# json.dump(hashes, open('hashes.json', 'w+'), indent=2)
