from skimage.metrics import structural_similarity as ssim
from skimage.metrics import peak_signal_noise_ratio as psnr
import matplotlib.pyplot as plt
import numpy as np
import cv2
import json
import sqlite3
from os import walk
from os.path import join
from tqdm import tqdm, trange
from tqdm.contrib.concurrent import process_map

conn = sqlite3.connect('example.db')
c = conn.cursor()

firstID = c.execute('''SELECT id FROM images order by id limit 1''').fetchone()[0]
lastID = c.execute('''SELECT id FROM images order by id desc limit 1''').fetchone()[0]


def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err

    '''
    fig = plt.figure(title)
    plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))
    # show first image
    ax = fig.add_subplot(1, 2, 1)
    plt.imshow(imageA, cmap=plt.cm.gray)
    plt.axis("off")
    # show the second image
    ax = fig.add_subplot(1, 2, 2)
    plt.imshow(imageB, cmap=plt.cm.gray)
    plt.axis("off")
    # show the images
    plt.show()
    '''


def getImageFromID(imageID):
    sql = '''SELECT filepath FROM images WHERE id=?'''
    return c.execute(sql, (imageID,)).fetchone()[0]


results = []
errors = []


def compareImageToAllLowerImages(firstImageID):
    firstImagePath = getImageFromID(firstImageID)
    firstImage = cv2.imread(firstImagePath)
    if firstImage is None:
        raise ValueError('Cant load: ' + firstImagePath)
    firstImageWidth, firstImageHeight, color = firstImage.shape
    for secondImageID in range(firstImageID, lastID):
        try:
            secondImagePath = getImageFromID(secondImageID)
            secondImage = cv2.imread(secondImagePath)
            if secondImage is None:
                raise ValueError('Cant load: ' + secondImagePath)
            secondImageWidth, secondImageHeight, color = secondImage.shape
            if firstImageWidth / firstImageHeight is not secondImageWidth / secondImageHeight:
                continue
            if firstImageWidth > secondImageWidth:
                changedFirstImage = cv2.resize(firstImage, (secondImageWidth, secondImageHeight))
            elif secondImageWidth > firstImageWidth:
                secondImage = cv2.resize(secondImage, (firstImageWidth, firstImageHeight))
                changedFirstImage = firstImage
            else:
                changedFirstImage = firstImage

            m, s = compare_images(changedFirstImage, secondImage)
            results.append((m, s))
        except:
            if secondImagePath is not None:
                errors.append(secondImagePath)

f = []

for (dirpath, dirnames, filenames) in walk('/run/media/jeremiasf/Linux/newMamaOutput/2009/(11) November'):
    for file in filenames:
        f.append(join(dirpath, file))

image_files = []

for file in f:
    if str(file).endswith('.jpg') or str(file).endswith('.JPG'):
        image_files.append(file)


def rotate_image(image):
    (h, w) = image.shape[:2]
    center = (w / 2, h / 2)
    angel = 90
    scale = 1.0
    M = cv2.getRotationMatrix2D(center, angel, scale)
    return cv2.warpAffine(image, M, (h, w))


def compare_images(image_a, image_b):
    (h1, w1) = image_a.shape[:2]
    for _ in range(4):
        (h2, w2) = image_b.shape[:2]
        if (h1 / w1 - h2 / w2) < 0.25:
            if w1 > w2:
                image_a_conv = cv2.resize(image_a, (w2, h2))
            elif w1 < w2:
                image_b = cv2.resize(image_b, (w1, h1))
                image_a_conv = image_a
            else:
                image_a_conv = image_a
            psnr_value = psnr(image_a_conv, image_b)
            if psnr_value > 25:
                return True
        image_b = cv2.rotate(image_b, cv2.ROTATE_90_CLOCKWISE)
    return False

def compare_image_to_list(args):
    image_a = cv2.imread(args[0])
    second_images = args[1]
    same_images = []
    for second_image in second_images:
        image_b = cv2.imread(second_image)
        if compare_images(image_a, image_b):
            same_images.append((args[0], second_image))
    return same_images



compare_results = []
same_images = []
compare_counter = 0

to_compare = []

for i in range(len(image_files)):
    first_image = image_files[i]
    second_images = []
    for j in range(i + 1, len(image_files)):
        second_images.append(image_files[j])
    to_compare.append([first_image, second_images])

results = process_map(compare_image_to_list, to_compare)

to_save = []
for result in results:
    if len(result) > 0:
        to_save.append(result[0])

json.dump(to_save, open('results.json', 'w+'))

# json.dump(compare_results, open('compare_results.json', 'w+'))
# json.dump(same_images, open('same_images.json', 'w+'))
# print(compare_counter)


# print(process_map(compareImageToAllLowerImages, range(firstID, lastID)))

# json.dump(results, open('results.json', 'w+'), indent=4)
# json.dump(errors, open("errors.json", 'w+'), indent=4)

# original = cv2.imread('background.png')
# original = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
# original = cv2.resize(original, (1920, 800))

# scaled = cv2.imread('background_scaled.png')
# scaled = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)
# print(scaled.shape)

# compare_images(original, original, "Original")
# compare_images(original, scaled, "Scaled")
