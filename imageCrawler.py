from os import walk, stat
from os.path import join
from PIL import Image
from PIL.ExifTags import TAGS
from tqdm import tqdm
import cv2
from filehash import FileHash
import json
from skimage import io

import datetime
import sqlite3

starting_dir = '/run/media/jeremiasf/Linux/MamaDvDs'

f = []

md5hasher = FileHash('md5')

for (dirpath, dirnames, filenames) in walk(starting_dir):
    for file in filenames:
        f.append(join(dirpath, file))

jpg_count = 0
avi_count = 0
different_count = 0
different_files = []
image_files = []
for file in f:
    if str(file).endswith('.jpg') or str(file).endswith('.JPG'):
        jpg_count += 1
        image_files.append(file)
    elif str(file).endswith('.avi'):
        avi_count += 1
    else:
        different_count += 1
        different_files.append(file)

# print(different_files)
print(len(f))
print('JPGs: ' + str(jpg_count))
print('AVIs: ' + str(avi_count))
print('Different: ' + str(different_count))

conn = sqlite3.connect('example.db')
c = conn.cursor()


def addImageToDatabase(toAddImage, path, file_info):
    ret = {}
    captured = ""
    try:
        info = toAddImage._getexif()
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            ret[decoded] = value
        if ret['DateTimeOriginal'] is not None:
            date_string, time_string = str(ret['DateTimeOriginal']).split(' ')
            date_array = date_string.split(':')
            time_array = time_string.split(':')
            captured = str(
                datetime.datetime(int(date_array[0]), int(date_array[1]), int(date_array[2]), int(time_array[0]),
                                  int(time_array[1]), int(time_array[2])).timestamp())
    except:
        pass

    if captured == '' or captured == '1009839600.0':
        captured = str(float(file_info[8]))

    width, height = single_img.size

    image_hash = md5hasher.hash_file(image)

    sql = '''INSERT INTO images (filepath, height, width, captured, hash) VALUES (?, ?, ?, ?, ?)'''
    values = (path, height, width, captured, image_hash)
    try:
        c.execute(sql, values)
    except:
        print('Insert Failed')


corruptedImages = []

for image in tqdm(image_files, desc='Database Image'):
    try:
        file_info = stat(image)
        single_img = Image.open(image)
        single_img.verify()
        _ = io.imread(image)
        # openCvImage = cv2.imread(image)
        if image is not None:
            addImageToDatabase(single_img, image, file_info)
        else:
            corruptedImages.append((image, 'OpenCV error'))
    except Exception as e:
        corruptedImages.append((image, str(e)))

json.dump(corruptedImages, open('imageCrawlerDeadFiles.json', 'w+'))

conn.commit()
conn.close()
