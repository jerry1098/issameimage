import matplotlib.pyplot as plt
import cv2
import json

same_images = json.load(open('results.json', 'r'))

print(len(same_images))

for same_image in same_images:
    image1 = cv2.imread(same_image[0])
    image2 = cv2.imread(same_image[1])

    fig = plt.figure('Test')
    # plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))
    # show first image
    fig.add_subplot(1, 2, 1)
    plt.imshow(image1)
    plt.axis("off")
    plt.title(str(image1.shape[0]) + ' x ' + str(image1.shape[1]))
    # show the second image
    fig.add_subplot(1, 2, 2)
    plt.imshow(image2)
    plt.axis("off")
    plt.title(str(image2.shape[0]) + ' x ' + str(image2.shape[1]))
    # show the images
    plt.show()
