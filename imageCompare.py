import sqlite3
import cv2
from tqdm import tqdm
import json

conn = sqlite3.connect('example.db')
c = conn.cursor()
firstID = c.execute('''SELECT id FROM images order by id limit 1''').fetchone()[0]
lastID = c.execute('''SELECT id FROM images order by id desc limit 1''').fetchone()[0]


def getImageFromID(imageID):
    sql = '''SELECT filepath FROM images WHERE id=?'''
    return c.execute(sql, (imageID,)).fetchone()[0]


failedList = []

for imageID in tqdm(range(firstID, lastID)):
    imagePath = getImageFromID(imageID)
    try:
        image = cv2.imread(imagePath)
        if image is None:
            failedList.append(imagePath)
    except Exception as e:
        print('Exception: ')
        print(e)
        failedList.append(imagePath)


print(len(failedList))
json.dump(failedList, open('corruptedFiles.json', 'w+'), indent=4)
